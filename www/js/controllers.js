angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats, MovieService) {
	$scope.chats = MovieService.load();
})

.controller('ChatDetailCtrl', function($scope, $stateParams, MovieService) {
  $scope.chat = MovieService.get($stateParams.chatId);
  
  
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
